Template.gallery.helpers({
  galleries: function() {
    return Galleries.find({
      collection: Iron.Location.get().path.slice(1)
    });
  }
});
