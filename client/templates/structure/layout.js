var gallerySeries = ["assemblages", "blue", "washes", "drawings", "fairytales", "gagra", "vibrations", "sea"];

Template.layout.helpers({
  displays: gallerySeries,
  isGallery: function() {
    if (gallerySeries.indexOf(Iron.Location.get().path.slice(1)) >= 0) {
      return true;
    }
  },

  menuOpen: function() {
    return Session.get('menu')
  }
});

Template.layout.events({

  'click .menu-button': function() {
    var menuButton = Session.get('menu')
    Session.set('menu', !menuButton);
  },

  'click .menu li a': function() {
    Session.set('menu', false);
  }
})
