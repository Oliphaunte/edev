Router.configure({
  layoutTemplate: 'layout',
  loadingTemplate: 'loading',
  notFoundTemplate: 'notFound'
});

Router.map(function() {
  this.route('index', {path: '/'} );
  this.route('cv');
  this.route('gift');
  this.route('statement');
  this.route('contact');
  this.route('gallery', {path: '/(assemblages|blue|washes|drawings|fairytales|gagra|vibrations|sea)'});
});
