Galleries = new Mongo.Collection(null);

var galleryData = [
  {
    id: 'assemblage-a-construct',
    title: 'A Construct',
    year: 2014,
    style: 'mixed-media',
    dimensions: '5 x 5 inches',
    dimensionsType: '1',
    image: 'assemblages/a-construct.jpg',
    collection: 'assemblages'
  }, {
    id: 'assemblage-city-and-valentina',
    title: 'City And Valentina',
    year: 2014,
    style: 'mixed-media',
    dimensions: '11 x 10 inches',
    dimensionsType: '2',
    image: 'assemblages/city-and-valentina.jpg',
    collection: 'assemblages'
  }, {
    id: 'assemblage-city-i',
    title: 'City i',
    year: 2014,
    style: 'mixed-media',
    dimensions: '11 x 10 inches',
    dimensionsType: '2',
    image: 'assemblages/city-i.jpg',
    collection: 'assemblages'
  }, {
    id: 'assemblage-east-coast-on-the-west-coast',
    title: 'East coast on the west coast',
    year: 2013,
    style: 'mixed-media',
    dimensions: '22 x 30 inches',
    dimensionsType: '8',
    image: 'assemblages/east-coast-on-the-west-coast.jpg',
    collection: 'assemblages'
  }, {
    id: 'assemblage-sorry-not-sorry-mr-bradford',
    title: 'Sorry not sorry Mr. Bradford',
    year: 2014,
    style: 'mixed-media on canvas',
    dimensions: '24 x 18 inches',
    dimensionsType: '2',
    image: 'assemblages/sorry-not-sorry-mr-bradford.jpg',
    collection: 'assemblages'
  }, {
    id: 'assemblage-untitled',
    title: 'Untitled',
    year: 2014,
    style: 'mixed-media',
    dimensions: '16 x 12 inches',
    dimensionsType: '2',
    image: 'assemblages/untitled.jpg',
    collection: 'assemblages'
  }, {
    id: 'assemblage-what-i-see-when-i-look-at-bridget-riley',
    title: 'What I see when I look at Bridget Riley',
    year: 2014,
    style: 'mixed-media',
    dimensions: '16 x 12 inches',
    dimensionsType: '2',
    image: 'assemblages/what-i-see-when-i-look-at-bridget-riley.jpg',
    collection: 'assemblages'
  }, {
    id: 'blue-i',
    title: 'Blue i',
    year: 2014,
    style: 'mixed-media',
    dimensions: '20 x 16 inches',
    dimensionsType: '6',
    image: 'blue/blue-i.jpg',
    collection: 'blue'
  }, {
    id: 'blue-ii',
    title: 'Blue ii',
    year: 2014,
    style: 'mixed-media',
    dimensions: '20 x 16 inches',
    dimensionsType: '6',
    image: 'blue/blue-ii.jpg',
    collection: 'blue'
  }, {
    id: 'blue-iii',
    title: 'Blue iii',
    year: 2014,
    style: 'mixed-media',
    dimensions: '20 x 16 inches',
    dimensionsType: '6',
    image: 'blue/blue-iii.jpg',
    collection: 'blue'
  }, {
    id: 'blue-iv',
    title: 'Blue iv',
    year: 2014,
    style: 'mixed-media',
    dimensions: '20 x 16 inches',
    dimensionsType: '6',
    image: 'blue/blue-iv.jpg',
    collection: 'blue'
  }, {
    id: 'fairytales-hugo',
    title: 'Hugo',
    year: 2014,
    style: 'mixed-media',
    dimensions: '22 x 30 inches',
    dimensionsType: '3',
    image: 'fairytales/hugo.jpg',
    collection: 'fairytales'
  }, {
    id: 'fairytales-little-prince-drawing',
    title: 'Little Prince Drawing',
    year: 2014,
    style: 'mixed-media',
    dimensions: '22 x 30 inches',
    dimensionsType: '9',
    image: 'fairytales/little-prince-drawing.jpg',
    collection: 'fairytales'
  }, {
    id: 'fairytales-little-prince',
    title: 'Little Prince',
    year: 2014,
    style: 'mixed-media',
    dimensions: '22 x 30 inches',
    dimensionsType: '3',
    image: 'fairytales/little-prince.jpg',
    collection: 'fairytales'
  }, {
    id: 'gagra-view-day-i',
    title: 'View Day i',
    year: 2014,
    style: 'watercolor',
    dimensions: '22 x 30 inches',
    dimensionsType: '10',
    image: 'gagra/view-day-i.jpg',
    collection: 'gagra'
  }, {
    id: 'gagra-view-day-ii',
    title: 'View Day ii',
    year: 2014,
    style: 'watercolor',
    dimensions: '22 x 30 inches',
    dimensionsType: '10',
    image: 'gagra/view-day-ii.jpg',
    collection: 'gagra'
  }, {
    id: 'gagra-view-day-iii',
    title: 'View Day iii',
    year: 2014,
    style: 'watercolor',
    dimensions: '22 x 30 inches',
    dimensionsType: '10',
    image: 'gagra/view-day-iii.jpg',
    collection: 'gagra'
  }, {
    id: 'gagra-view-morning-i',
    title: 'View Morning i',
    year: 2013,
    style: 'watercolor',
    dimensions: '22 x 30 inches',
    dimensionsType: '10',
    image: 'gagra/view-morning-i.jpg',
    collection: 'gagra'
  }, {
    id: 'gagra-view-night-i',
    title: 'View Night i',
    year: 2013,
    style: 'watercolor',
    dimensions: '22 x 30 inches',
    dimensionsType: '10',
    image: 'gagra/view-night-i.jpg',
    collection: 'gagra'
  }, {
    id: 'black-sea',
    title: 'Black Sea',
    year: 2013,
    style: 'watercolor',
    dimensions: '22 x 30 inches',
    dimensionsType: '4',
    image: 'seas/black-sea.jpg',
    collection: 'sea'
  }, {
    id: 'sea-i-never-see',
    title: 'Sea I Never See',
    year: 2013,
    style: 'watercolor',
    dimensions: '7 x 4 feet',
    dimensionsType: '4',
    image: 'seas/sea-i-never-see.jpg',
    collection: 'sea'
  }, {
    id: 'sun-down',
    title: 'Sun Down',
    year: 2013,
    style: 'watercolor',
    dimensions: '22 x 30 inches',
    dimensionsType: '4',
    image: 'seas/sun-down.jpg',
    collection: 'sea'
  }, {
    id: 'vibration-1',
    title: 'A-Major',
    year: 2013,
    style: 'watercolor',
    dimensions: '18 x 24 inches',
    dimensionsType: '5',
    image: 'vibrations/A-Major.jpg',
    collection: 'vibrations'
  }, {
    id: 'vibration-2',
    title: 'B-Minor',
    year: 2013,
    style: 'watercolor',
    dimensions: '18 x 24 inches',
    dimensionsType: '5',
    image: 'vibrations/B-Minor.jpg',
    collection: 'vibrations'
  }, {
    id: 'wash-blue-i',
    title: 'Wash Blue i',
    year: 2014,
    style: 'watercolor',
    dimensions: '9 x 12  inches',
    dimensionsType: '7',
    image: 'washes/wash-blue-i.jpg',
    collection: 'washes'
  }, {
    id: 'wash-blue-ii',
    title: 'wash-blue-ii',
    year: 2014,
    style: 'watercolor',
    dimensions: '9 x 12  inches',
    dimensionsType: '7',
    image: 'washes/wash-blue-ii.jpg',
    collection: 'washes'
  }, {
    id: 'wash-blue-iii',
    title: 'wash-blue-iii',
    year: 2014,
    style: 'watercolor',
    dimensions: '9 x 12  inches',
    dimensionsType: '7',
    image: 'washes/wash-blue-iii.jpg',
    collection: 'washes'
  }, {
    id: 'wash-blue-iv',
    title: 'wash-blue-iv',
    year: 2014,
    style: 'watercolor',
    dimensions: '9 x 12  inches',
    dimensionsType: '7',
    image: 'washes/wash-blue-iv.jpg',
    collection: 'washes'
  }, {
    id: 'wash-blue-v',
    title: 'A-Major',
    year: 2014,
    style: 'watercolor',
    dimensions: '9 x 12  inches',
    dimensionsType: '7',
    image: 'washes/wash-blue-vii.jpg',
    collection: 'washes'
  }, {
    id: 'wash-blue-vi',
    title: 'wash-blue-vi',
    year: 2014,
    style: 'watercolor',
    dimensions: '9 x 12  inches',
    dimensionsType: '7',
    image: 'washes/wash-blue-vi.jpg',
    collection: 'washes'
  }, {
    id: 'wash-blue-vii',
    title: 'wash-blue-vii',
    year: 2014,
    style: 'watercolor',
    dimensions: '9 x 12  inches',
    dimensionsType: '7',
    image: 'washes/wash-blue-vii.jpg',
    collection: 'washes'
  }, {
    id: 'wash-blue-viii',
    title: 'A-Major',
    year: 2014,
    style: 'watercolor',
    dimensions: '9 x 12  inches',
    dimensionsType: '7',
    image: 'washes/wash-blue-viii.jpg',
    collection: 'washes'
  }, {
    id: 'wash-blue-ix',
    title: 'wash-blue-ix',
    year: 2014,
    style: 'watercolor',
    dimensions: '9 x 12  inches',
    dimensionsType: '7',
    image: 'washes/wash-blue-ix.jpg',
    collection: 'washes'
  }, {
    id: 'another-hot-spot',
    title: 'Another Hot Spot',
    year: 2014,
    style: 'mixed media on paper',
    dimensions: '8 x 10 inches',
    dimensionsType: '11',
    image: 'drawings/another-hot-spot.jpg',
    collection: 'drawings'
  }, {
    id: 'crimson-sails',
    title: 'Crimson Sails',
    year: 2014,
    style: 'mixed media on paper',
    dimensions: '8 x 10 inches',
    dimensionsType: '11',
    image: 'drawings/crimson-sails.jpg',
    collection: 'drawings'
  }, {
    id: 'entertain-us',
    title: 'Entertain Us',
    year: 2014,
    style: 'mixed media on paper',
    dimensions: '8 x 10 inches',
    dimensionsType: '11',
    image: 'drawings/entertain-us.jpg',
    collection: 'drawings'
  }, {
    id: 'no-longer-than-2-feet',
    title: 'No Longer Than 2 Feet',
    year: 2014,
    style: 'mixed media on paper',
    dimensions: '8 x 10 inches',
    dimensionsType: '11',
    image: 'drawings/no-longer-than-2-feet.jpg',
    collection: 'drawings'
  }, {
    id: 'untitled',
    title: 'Untitled',
    year: 2014,
    style: 'mixed media on paper',
    dimensions: '8 x 10 inches',
    dimensionsType: '11',
    image: 'drawings/untitled.jpg',
    collection: 'drawings'
  }, {
    id: 'smoking-the-dreams-away',
    title: 'Smoking The Dreams Away',
    year: 2014,
    style: 'mixed media on paper',
    dimensions: '8 x 10 inches',
    dimensionsType: '11',
    image: 'drawings/smoking-the-dreams-away.jpg',
    collection: 'drawings'
  }, {
    id: 'you-and-me',
    title: 'You And Me',
    year: 2014,
    style: 'mixed media on paper',
    dimensions: '8 x 10 inches',
    dimensionsType: '11',
    image: 'drawings/you-and-me.jpg',
    collection: 'drawings'
  }
];

galleryData.forEach(function(gallery){
  Galleries.insert(gallery);
});
